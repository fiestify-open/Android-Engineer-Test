## What you need to know first:
**Welcome!** This is a small exercise that aims to demonstrate your skills in:

 - Java
 - Android
 - Web Services
 - Off-line communication (docs, commits messages, etc.)

It is important that the code is maintainable and well tested using your preferred testing framework. The goal is to automatically validate that the application does what is asked for in the requirements.

## What you're supposed to do:

We want to develop an app that allows you to read the news of the last week of the newspaper 'The Guardian' (how you design the interface, is up to you). In order to do this, you will use The Guardian's open API, which allows you to obtain an API Key through a quick signup to perform operations against your API. 

To do so, register as 'Developer' at this link: http://open-platform.theguardian.com/access/

The app must:

 - Show all news from the last week on a home screen
 - Search for news related to a particular term (e. g.,"politics")

The API documentation explains how to access this information: http://open-platform.theguardian.com/documentation/

## Instructions & How you should submit your exercise:

 1. Clone the Git repository
 2. Work on the exercise
 3. When you finish, along with the code, add the changes to the README.md with the necessary instructions to run the application.
 4. Make a **Merge Request** with the changes you want to submit when you finish your work.

Thanks for your time!
